-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 16, 2016 at 08:27 PM
-- Server version: 10.0.21-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `AshHost`
--

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `PhoneNumber` int(10) NOT NULL,
  `Gender` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`FirstName`, `LastName`, `Email`, `PhoneNumber`, `Gender`) VALUES
('Dr. Ayokor', 'Korsah', 'a.k@ashesi.edu.gh', 7890, 0),
('Anthony', 'Abeo', 'abio.uyiok@asuja.com', 4567890, 0),
('Hector', 'Amoah', 'hectoramoah@yahoo.com', 67890, 0),
('Well', 'Inton', 'inton@iuajs.com', 67890, 0),
('Leon', 'Ampah', 'leon.ampah@ashesi.edu.gh', 4657890, 0),
('mike', 'mane', 'mike.make@ashesi.edu.gh', 345678, 0),
('mike', 'name', 'mike.name@ashesi.edu.gh', 2345, 0),
('Nii', 'Apa', 'nii.apa@ashesi.edu.gh', 5467890, 0),
('Romel', 'Netteu', 'romel@gmail.com', 6789, 0),
('Ronald', 'Nettey', 'ronald.nettey@ashesi.edu', 6587, 0),
('Ronald ', 'Nettey', 'ronaldnettey@yaho.com', 265578245, 0),
('Json', 'Doe', 'ronaldnettey@yahoo.com', 768, 0),
('Scooby', 'Doo', 'scooby.doo@ashesi.com', 789, 0),
('rtyui', 'tyui', 'yui@iausjk.con', 478, 0),
('kskdm', 'kmwd', '`kmeqe!2kmw@ijsd.com', 8790, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`Email`),
  ADD KEY `Email` (`Email`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
