<%-- 
    Document   : UserPage_Dto
    Created on : Dec 4, 2016, 1:56:38 PM
    Author     : CYBRON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="css/style.css">
        <link rel ="stylesheet" href="css/confirm.css">
        <title>Ash Host</title>
    </head>
    <body>
        <section class ="container">
            <section class ="wrapper">
                <section class ="headerM">
                    <h1>${user.getFirstName()} You've Been Added to the Waiting List</h1>
                    <p>Proceed To Payment To Secure Your Room</p>
                </section>
                <section class ="buttonS">
                    <button>Proceed To Payment</button>
                    <button>Maybe Later</button>
                </section>
            </section>
        </section>
       
    </body>
</html>
