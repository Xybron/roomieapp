<%-- 
    Document   : Dashboard
    Created on : Dec 4, 2016, 7:31:05 PM
    Author     : CYBRON
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.yhupmine.almostthere.DatabaseModel"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel ="stylesheet" href="css/style.css">
        <link rel ="stylesheet" href="css/confirm.css">
        <link rel ="stylesheet" href="css/dashboard.css">
        <title>Dashboard | Admin</title>
    </head>
    <body>
        <section class ="container">
            <section class ="wrapper">
                <header class ="dashHeader"></header>
                <section class ="adminControl">
                    <aside class="leftPane">
                        <ul>
                            <li><i class="fa fa-home" aria-hidden="true"></i></li>
                        </ul>
                    </aside>
                    <section class ="rightPane">
                        <section class ="innerContainer">
                            <section id = "overL">
                                <ul>
                                    <li><i class="fa fa-minus-circle" aria-hidden="true"></i></li>
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </ul>
                            </section>
                            <%  

                               DatabaseModel userDB = new DatabaseModel();
                               userDB.startDatabase();

                               Statement statement = userDB.getDBConnection().createStatement();

                               ResultSet resultset = statement.executeQuery("select * from Users");


                           %>
                           <section class ="userDeets">
                              
                            <table>

                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email Address</th>
                                    <th>Phone Number</th>


                                 <%  while(resultset.next()){ %>
                                 <tr onclick = "rowSelect(this)">
                                
                                     <td> <%= resultset.getString(1) %> </td>
                                     <td><%= resultset.getString(2) %> </td>
                                     <td><%= resultset.getString(3) %> </td>
                                     <td><%= resultset.getInt(4) %> </td>
                                 </tr>

                            <%  }  %>



                            </table> 
                           </section>
                        </section>
                    </section>
                </section>
            </section>
        </section>
                            
                            <script>
                                var par = document.getElementById('overL');
                                function rowSelect(e){
                                  
                                  par.classList.add('overlay');
                                  e.classList.add('sRow');
                                  e.style.position = "absolute";
                                  e.style.backgroundColor = "white";
                                  e.style.color = "#4B111C";
                                }
                                
                                function removeElem(e){
                                    e.remove();
                                }
                            </script>

    </body>
</html>
