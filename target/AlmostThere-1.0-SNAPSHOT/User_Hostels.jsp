<%-- 
    Document   : User_Hostels
    Created on : Dec 4, 2016, 1:57:18 PM
    Author     : CYBRON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel ="stylesheet" href="css/style.css">
        <link rel ="stylesheet" href="css/hostel.css">
        <title>Hostels Page</title>
    </head>
    <body>
       <section class ="container">
        <section class ="header">
             <h1>Select Hostel</h1>
             <p>Kindly select your preferred hostel.</p>
        </section>
         <section class ="hostelList">
             <ul>
                 <li id = "li1" onclick = "openWin(1)">
                     <div class ="hostel backgroundCover">
                         <button>Dufie</button>
                         <p>#Available Rooms : ""</p>
                     </div>
                 </li>
                 <li onclick = "openWin(2)">
                     <div class ="hostel backgroundCover ">
                         <button>Hozanna</button>
                         <p>#Available Rooms : ""</p>
                     </div>
                 </li>
                 <li onclick = "openWin(3)">
                     <div class="hostel backgroundCover ">
                         <button>Chalot</button>
                         <p>#Available Rooms : ""</p>
                     </div>
                 </li>
                 <li onclick = "openWin(4)">
                     <div class ="hostel backgroundCover ">
                         <button>QueenStar</button>
                         <p>#Available Rooms : ""</p>
                     </div>
                 </li>
                 <li onclick = "openWin(5)">
                     <div class ="hostel backgroundCover ">
                         <button>Columbiana</button>
                         <p>#Available Rooms : ""</p>
                     </div>
                 </li>
                 <li onclick = "openWin(6)">
                     <div class ="hostel backgroundCover ">
                         <button>Masere</button>
                         <p>#Available Rooms : ""</p>
                     </div>
                 </li>

             </ul>
         </section>
       </section>
        
        <section id = "formWin" class ="container userForm">
            
            <section class ="hostelPoster" id ="picHold"></section>
            <section  class ="hostelForm">
                <div class ="closeBtn">
                <i class="fa fa-times"  id ="closeBttn" onclick = "closeWin()" aria-hidden="true"></i>
            </div>
                 <form action ="userDeets" method ="post">
                    <input type ="text" placeholder ="First Name" name ="firstName" required><br>
                    <input type ="text" placeholder ="Last Name" name ="lastName" required><br>
                    <input type ="email" placeholder="Email Address" name ="email" required><br>
                    <select class = "selectG">                      
                        <option>Male</option>
                        <option>Female</option>
                    </select><br>
                    <input id = "tel" type ="tel" placeholder="Contact Number" name ="mobile" required><br>
                    
         
            <button class = "nxtButton btnEnter" type ="submit">Get Room</button>
        </form>
            </section>
        </section>
        
        <script>
          
               var e = document.getElementById('formWin');
               var f = document.getElementById('picHold');
            function closeWin(){
              
                e.style.display = "none";
            }
            
            function openWin(g){
                 //var picHold = document.getElementById(f);
                 f.style.backgroundSize = "cover";
                 f.style.backgroundPosition = "center";
                 
                 if(g === 1)
                     f.style.backgroundImage = "url(images/house_targaryen.jpg)";
                     
                 else if(g === 2)
                     f.style.backgroundImage = "url(images/growing-strong-house-tyrell.jpeg)";
                 
                 else if(g === 3)
                     f.style.backgroundImage = "url(images/honor_house_arryn.jpg)";
                 
                 else if(g === 4)
                     f.style.backgroundImage = "url(images/roar.jpg)";
                
                 else if(g === 5)
                     f.style.backgroundImage = "url(images/family_duty_honor_house_tully.jpg)";
                 
                 else if(g === 6)
                     f.style.backgroundImage = "url(images/we_do_not_sow_house_greyjoy.jpg)";
                 
                
                 
                 e.style.display = "flex";
            }
            
            
        </script>
        
        
    </body>
</html>
