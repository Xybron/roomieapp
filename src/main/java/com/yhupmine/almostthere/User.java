/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yhupmine.almostthere;

/**
 *
 * @author CYBRON
 */
public class User {
    private String firstName;
    private String lastName;
    private String email;
    private int phoneNumber;
    private boolean gender;
    
    public User(){
        this(null,null,null,0);
    }
    
    public User(String fn, String ln, String em, int pn){
        firstName = fn;
        lastName = ln;
        email = em;
        phoneNumber = pn;
        gender = false; //false is female
       
    }
    
    public void setFirstName(String name){
        firstName = name;
    }
    
    public void setLastName(String name){
        lastName = name;
    }
    
    public void setEmail(String eml){
        email = eml;
    }
    
    public void setGender(boolean g){
        gender = g;
    }
    
    public void setPhoneNumber(int number){
       phoneNumber = number;
    }
    
    public String getFirstName(){
        return firstName;
    }
    
    public String getLastName(){
        return lastName;
    }
    
    public String getEmail(){
        return email;
    }
    
    public int getPhoneNumber(){
        return phoneNumber;
    }
    
    public boolean getGender(){
        return gender;
    }
}
