/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yhupmine.almostthere;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author CYBRON
 */
public class Hostel {
    private String hostelName;
    private HashMap<String,Room> rooms;
    private int numberOfRooms;
    
    public Hostel(){
        this(null,0);
    }
    
    public Hostel(String hlName, int numRooms){
        hostelName = hlName;
        numberOfRooms = numRooms;
        rooms = new HashMap<String,Room>();
    }
    
    public void setHostelName(String hN){
        hostelName = hN;
    }
    
    public void setNumberOfRooms(int nR){
        numberOfRooms = nR;
    }
    
    public String getHostelName(){
        return hostelName;
    }
    
    public int getNumberOfRooms(){
        return numberOfRooms;
    }
    
    //public boolean add
}
