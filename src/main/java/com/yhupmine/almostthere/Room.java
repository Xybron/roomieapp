/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yhupmine.almostthere;

import java.util.HashSet;

/**
 *
 * @author CYBRON
 */
public class Room {
    private String roomID;
    private boolean genderCategory; //Whether the room is for males(true) or females(false)
    private int numberOfRoom_mates; //4 Max;
    private boolean reserved;
    private boolean booked;
    private HashSet<User> occupants;
    
    public Room(){
        this(null,false,0,false,false);
    }
    
    public Room(String rID, boolean M_F, int numRoom_mates, boolean rsvp, boolean bkd){
        roomID = rID;
        genderCategory = M_F;
        numberOfRoom_mates = numRoom_mates;
        reserved = rsvp;
        booked = bkd;
        occupants = new HashSet<User>();
    }
    
    public void setRoomID(String id){
        roomID = id;
    }
    
    public void setGenderCategory(boolean gCat){
        genderCategory = gCat;
    }
    
    public void setNumberOfRoomMates(int nR){
        numberOfRoom_mates = nR;
    }
     
    public void setReserveRoom(boolean rR){
        reserved = rR;
    }
    
    public void setBookedRoom(boolean bR){
        booked = bR;
    }
    
    public String getRoomID(){
       return roomID;
    }
    
    public boolean getGenderCategory(){
        return genderCategory;
    }
    
    public int getNumberOfRoomMates(){
        return numberOfRoom_mates;
    }
     
    public boolean getReserveRoom(){
        return reserved;
    }
    
    public boolean getBookedRoom(){
        return booked;
    }
    
    public HashSet<User> getOccupants(){
        return occupants;
    }
    
    public boolean addOccupant(User nU){
        return(getOccupants().add(nU));
    }
    
    public boolean removeOccupant(User nU){
        return(getOccupants().remove(nU));
    }
    


    
}


