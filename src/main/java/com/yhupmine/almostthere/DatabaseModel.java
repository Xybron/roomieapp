/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yhupmine.almostthere;

import java.sql.*;

/**
 *
 * @author CYBRON
 */
public class DatabaseModel {
    private String username; 
    private String password;
    private String jdbc_driver; 
    private String db_url;
    private Connection conn;
    
    public DatabaseModel(){
        this("root","cyberon60",null);
    }
    
    public DatabaseModel(String uN, String pW, Connection c){
        username = uN;
        password = pW;
        jdbc_driver = "com.mysql.jdbc.Driver";
        db_url = "jdbc:mysql://localhost/AshHost";
        conn = c;
    }
    public void setUserName(String un){
        username = un;
    }
    
     public void setPassWord(String pw){
       password = pw;
    }
     
     public void setConnection(Connection cn){
         conn = cn;
     }
     
     public Connection getDBConnection(){
         return conn;
     }
     
     public String getDBUrl(){
         return db_url;
     }
     
     public String getDBDriver(){
         return jdbc_driver;
     }
    public void startDatabase(){
       	try {
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	conn = java.sql.DriverManager.getConnection(
	"jdbc:mysql://localhost/AshHost?user=AshHost&password=offcampus");
	
	}
	catch (Exception e) {
		System.out.println(e);
		System.exit(0);
	}	
	System.out.println("Connection established");
	
    }
    
    public void addToDB(User u){
        try{
        PreparedStatement p=conn.prepareStatement("Insert Into Users set FirstName=?, LastName=? ,Email =?, PhoneNumber =?, Gender =?");
        p.setString(1, u.getFirstName());
        p.setString(2, u.getLastName());
        p.setString(3, u.getEmail());
        p.setInt(4, u.getPhoneNumber());
        p.setBoolean(5, u.getGender());
        
        p.execute();  //use execute if no results expected back
        }catch(Exception e){
            System.out.println("Error"+e.toString());
        return;        
    }
    }
    
    public void updateUser(User u){
        
    }
    
}
