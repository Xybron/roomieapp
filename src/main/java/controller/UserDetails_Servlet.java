/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import com.yhupmine.almostthere.DatabaseModel;
import com.yhupmine.almostthere.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import javax.servlet.RequestDispatcher;

/**
 *
 * @author CYBRON
 */
@WebServlet(name = "UserDetails_Servlet", urlPatterns = {"/userDeets"})
public class UserDetails_Servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String user_firstName, user_lastName, user_email;
        int user_phoneNumber;
        boolean gender;
        
        user_firstName = request.getParameter("firstName");
        user_lastName = request.getParameter("lastName");
        user_phoneNumber = Integer.parseInt(request.getParameter("mobile"));
        user_email = request.getParameter("email");
        
       // System.out.println(user_firstName + " " + user_lastName + " " + user_phoneNumber + " " + user_email);
        
        User newUser = new User(user_firstName,user_lastName,user_email,user_phoneNumber);
        
        DatabaseModel userDB = new DatabaseModel();
        userDB.startDatabase();
        userDB.addToDB(newUser);
        
        request.setAttribute("user", newUser);
        request.getRequestDispatcher("UserPage_Dto.jsp").forward(request,response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
