<%-- 
    Document   : index
    Created on : Dec 4, 2016, 5:22:40 AM
    Author     : CYBRON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="css/style.css">
        <link rel ="stylesheet" href="css/homePage.css">
        <title>Almost</title>
    </head>
    <body>
        <section class ="container">
            <section class ="wrapper">
            <section class ="content">
                <h1 class = "title">ROOME</h1>
                <button class = "nxtButton" onclick = "changePage('User_Hostels.jsp')">Live Off-Campus</button><br>
                <button class = "nxtButton" style = "color : #E43F6F; border-color : #E43F6F" onclick = "changePage('Dashboard.jsp')">Manage A Hostel</button>
            </section>
       
        <script>
            function changePage(url){
                window.location = url;
            }
           
        </script>
    </body>
</html>
