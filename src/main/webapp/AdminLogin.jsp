<%-- 
    Document   : EmployeeLogin
    Created on : Dec 4, 2016, 7:25:13 PM
    Author     : CYBRON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin | Login</title>
    </head>
    <body>
        <h1>Welcome To Ash Host.</h1>
        <p>Please Sign In</p>
        
        <form action ="login" method ="post">
            <input type ="email" placeholder ="Email Address" name = "admin_email" required><br>
            <input type ="password" placeholder ="Password" name = "admin_password" required><br>
            <button type ="submit">Login</button>
            <p>Or</p>
            <a href="AdminSignUp.jsp">SignUp</a>
        </form>
    </body>
</html>
